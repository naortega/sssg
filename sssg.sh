#!/bin/bash

# Copyright (C) 2020  Ortega Froysa, Nicolás <nicolas@ortegas.org>
# Author: Ortega Froysa, Nicolás <nicolas@ortegas.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function parse {
	echo -n "Generating _site/$1 ... "
	touch _site/$1
	# get the variables
	last_find="$(grep -n "^%%%$" $1)"
	vars=()
	if ! [ -z $last_find ]
	then
		end_line="$(expr ${last_find%%:*} - 1)"
		for (( i=1; i<=$end_line; i++ ))
		do
			vars+=("$(sed "$i!d" $1)")
		done
	fi

	# import templates
	for i in $(grep -n "^#/" $1)
	do
		end_line="$(expr ${i%%:*} - 1)"
		if [ -z $last_find ]
		then
			sed -n "1,$end_line{p}" $1 >> _site/$1
		else
			start_line="$(expr ${last_find%%:*} + 1)"
			if [ $start_line -le $end_line ]
			then
				sed -n "$start_line,$end_line{p}" $1 >> _site/$1
			fi
		fi
		inc_file=${i#*:}
		cat _templates/${inc_file:2} >> _site/$1
		last_find="$i"
	done
	start_line="$(expr ${last_find%%:*} + 1)"
	sed -n "$start_line,\$p" $1 >> _site/$1

	# replace variables
	for i in "${vars[@]}"
	do
		var_name=${i%%=*}
		var_contents=${i#*=}
		sed -i "s/\${$var_name}/$var_contents/g" _site/$1
	done

	echo "OK"
}

function generate {
	# create directories
	for i in $(find . -type d -not -path "./_templates" \
		-not -path "./_site" -not -path "./_templates/*" \
		-not -path "./_site/*")
	do
		dir_path=${i:2}
		echo -n "Creating directory _site/$dir_path ... "
		mkdir -p _site/$dir_path
		echo "OK"
	done

	# generate files
	for i in $(find . -type f -not -path "./_templates/*" -not -path \
		"./_site/*")
	do
		file_path=${i:2}
		if [ "$file_path" -nt "_site/$file_path" ]
		then
			if [ ${file_path: -4} == ".htm" ] ||
				[ ${file_path: -4} == ".HTM" ] ||
				[ ${file_path: -5} == ".html" ] ||
				[ ${file_path: -5} == ".HTML" ]
			then
				# parse HTML/CSS files
				parse $file_path
			else
				# files that don't need parsing
				echo -n "Copying file to _site/$file_path ... "
				cp $file_path _site/$file_path
				echo "OK"
			fi
		fi
	done
}

generate
